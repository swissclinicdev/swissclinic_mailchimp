<?php
/**
 * Swiss Clinic
 *
 * @category    Swissclinic
 * @package     Swissclinic_MailChimp
 *
 * https://github.com/mailchimp/mc-magento2/commit/645a10d087d77b2fc0876aa7c2ef84b2cde544d4
 */

namespace Swissclinic\MailChimp\Helper;

use Ebizmarts\MailChimp\Helper\Data as OriginalDataHelper;

class Data extends OriginalDataHelper
{

    public function getMapFields($storeId = null)
    {
        if (!$this->_mapFields) {
            $customerAtt = $this->getCustomerAtts();
            $data = $this->getConfigValue(self::XML_MERGEVARS, $storeId);
            try {
                $data = $this->unserialize($data);
                if (is_array($data)) {
                    foreach ($data as $customerFieldId => $mailchimpName) {
                        $this->_mapFields[] = [
                            'mailchimp' => strtoupper($mailchimpName),
                            'customer_field' => $customerAtt[$customerFieldId]['attCode'],
                            'isDate' => $customerAtt[$customerFieldId]['isDate'],
                            'isAddress' => $customerAtt[$customerFieldId]['isAddress'],
                            'options' => $customerAtt[$customerFieldId]['options']
                        ];
                    }
                }
            } catch (\Exception $e) {
                $this->log($e->getMessage());
            }
        }
        return $this->_mapFields;
    }

    public function getSubscriberInterest($subscriberId, $storeId, $interest = null)
    {
        if (!$interest) {
            $interest = $this->getInterest($storeId);
        }
        /**
         * @var $interestGroup \Ebizmarts\MailChimp\Model\MailChimpInterestGroup
         */
        $interestGroup = $this->_interestGroupFactory->create();
        $interestGroup->getBySubscriberIdStoreId($subscriberId, $storeId);
        $serialized = $interestGroup->getGroupdata();
        if ($serialized) {
            try {
                $groups = $this->unserialize($serialized);
                if (isset($groups['group'])) {
                    foreach ($groups['group'] as $key => $value) {
                        if (isset($interest[$key])) {
                            if (is_array($value)) {
                                foreach ($value as $groupId) {
                                    foreach ($interest[$key]['category'] as $gkey => $gvalue) {
                                        if ($gvalue['id'] == $groupId) {
                                            $interest[$key]['category'][$gkey]['checked'] = true;
                                        } elseif (!isset($interest[$key]['category'][$gkey]['checked'])) {
                                            $interest[$key]['category'][$gkey]['checked'] = false;
                                        }
                                    }
                                }
                            } else {
                                foreach ($interest[$key]['category'] as $gkey => $gvalue) {
                                    if ($gvalue['id'] == $value) {
                                        $interest[$key]['category'][$gkey]['checked'] = true;
                                    } else {
                                        $interest[$key]['category'][$gkey]['checked'] = false;
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (\Exception $e) {
                $this->log($e->getMessage());
            }
        }
        return $interest;
    }
}